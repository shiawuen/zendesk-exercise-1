
## Coding exericse for Zendesk

Exercise instructions: https://gist.github.com/timotheeg/bea81b3a20213031b294c60341d83a22

### Prerequisites

You need to have [Node.js](https://nodejs.org/en/) installed on your computer.
You need to have your Terminal ready to execute some command line instructions.

### Exercise 1: String compression

Implementation at `lib/compress.js`
Test cases at `compress-test.js`

To run the test execute `npm run test-compress` in your terminal

### Exercise 2: URI comparison

Implementation at `lib/check-uris.js`
Test cases at `check-uris-test.js`

To run the test execute `npm run test-check-uris` in your terminal
