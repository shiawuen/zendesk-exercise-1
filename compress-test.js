const assert = require('assert')
const compress = require('./lib/compress')

const test = (source, expected) => {
  const result = compress(source)
  console.log('source: %s, result: %s, matched: %s',
    source,
    result,
    result === expected)
  assert(result === expected)
}

console.log('Testing compress function')
test('aaaabbaaaababbbcccccccccccc', 'a4b2a4b1a1b3c12')
test('ddddddddddddeeeeeeerscsduuuuuuee', 'd12e7r1s1c1s1d1u6e2')
test('', '')
test(new String(''), '')
test(null, '')
test([], '')
console.log('-------------------------------')
console.log('')