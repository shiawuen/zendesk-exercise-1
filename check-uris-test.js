const assert = require('assert')
const checkURIs = require('./lib/check-uris')
const URIParser = checkURIs.URIParser

const test = ({ uri1, uri2, matched }) => {
  (() => {
    const result = checkURIs(uri1, uri2)
    console.log('uri1: %s, normalized: %s', uri1, new URIParser(uri1).stringify())
    console.log('uri2: %s, normalized: %s', uri2, new URIParser(uri2).stringify())
    console.log('expected: %s, actual: %s', matched, result)
    assert(matched === result)
    console.log('')
  })()
}

console.log('Testing checkURIs function')

test({
  uri1: 'http://abc.com:80/~smith/home.html',
  uri2: 'http://ABC.com/%7Esmith/home.html',
  matched: true
})

test({
  uri1: 'http://abc.com/drill/down/foo.html',
  uri2: 'http://abc.com/drill/further/../down/./foo.html',
  matched: true
})
test({
  uri1: 'http://abc.com/foo.html?a=1&b=2',
  uri2: 'http://abc.com/foo.html?b=2&a=1',
  matched: true
})

test({
  uri1: 'http://abc.com/foo.html?a=1&b=2&a=3',
  uri2: 'http://abc.com/foo.html?a=3&a=1&b=2',
  matched: false
})

test({
  uri1: 'http://uname:passwd@host.com/foo/bar.html',
  uri2: 'http://uname:PAsswd@host.com/foo/bar.html',
  matched: false
})

console.log('-------------------------------')
console.log('')