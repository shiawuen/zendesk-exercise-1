module.exports = (str) => {
  if (!(typeof str == 'string' || str instanceof String)) {
    return ''
  }
  let results = []
  let count = 0
  for (let i = 0; i < str.length; i++) {
    count += 1
    if (str[i] === str[i + 1]) continue
    results.push(`${str[i]}${count}`)
    count = 0
  }
  return results.join('')
}
