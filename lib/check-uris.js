
const DEBUG = process.env.DEBUG ? true : false

const log = (...args) => {
  if (!DEBUG) return
  console.log(...args)
}

const PROTOCOLS = ['http://', 'https://']

class URIParser {
  constructor (str) {
    this.source = str
    this.uri = {
      protocol: '',
      username: '',
      password: '',
      host: '',
      port: '',
      pathname: '',
      query: {}
    }

    this._start = 0
    for (let i = 0; i <= str.length; i++) {
      let current = str.substring(this._start, i + 1)
      this.parseProtocol(current, i) &&
      this.parseHostAndPortAndAuth(current, i) &&
      this.parsePathname(current, i) &&
      this.parseQuerystring(current, i)
    }
  }

  parseProtocol (str, index) {
    str = str.toLowerCase()
    // skip if host already set
    if (this.uri.protocol) return true
    if (!PROTOCOLS.includes(str)) return
    log('-- set protocol:', str)
    this.uri.protocol = str.replace('//', '')
    this.setStartIndex(index)
  }

  parseHostAndPortAndAuth(str, index) {
    // skip if host already set
    if (this.uri.host) return true
    if (this.source[index + 1] !== '/') return
    log('-- set host, port, username, and password:', str)
    let [ auth, _host ] = str.replace('/', '').split('@')
    if (!_host) {
      _host = auth
      auth = ':'
    }
    let [ username, password ] = auth.split(':')
    let [ host, port = '80' ] = _host.toLowerCase().split(':')
    Object.assign(this.uri, { port, username, password, host, })
    this.setStartIndex(index)
  }

  parsePathname (str, index) {
    if (this.uri.pathname) return true

    const shouldParse = index === this.source.length ||
      ['#', '?'].includes(this.source[index + 1])
    if (!shouldParse) return

    log('-- set pathname:', str)

    this.uri.pathname = decodeURIComponent(str.toLowerCase())
      .split('/')
      .reduce((arr, x) => {
        if (x === '.') return arr
        if (x === '..') return arr.slice(0, arr.length - 1)
        return arr.concat(x)
      }, [])
      .join('/')
    this.setStartIndex(index)
  }

  parseQuerystring (str, index) {
    if (Object.keys(this.uri.query).length) return true
    const shouldParse = index === this.source.length ||
      this.source[index + 1] === '#'
    if (!shouldParse) return
    log('-- set query:', str)
    const query =  str.slice(1)
      .split('&')
      .map(set => set.split('='))
      .reduce((query, [key, value]) => Object.assign(query, { [key]: value }), {})
    Object.assign(this.uri.query, query)
    this.setStartIndex(index)
  }

  setStartIndex (index) {
    this._start = index + 1
  }

  stringify () {
    const { protocol, port, username, password, host, pathname, query } = this.uri
    const search = Object.keys(query)
      .sort()
      .map(key => `${key}=${encodeURIComponent(query[key])}`)
      .join('&')
    return [
      `${protocol}//`,
      username ? `${username}:${password}@` : '',
      host,
      port !== '80' ? `:${port}` : '',
      pathname,
      search ? `?${search}` : ''
    ].join('')
  }
}

module.exports = exports = (uri1, uri2) => {
  uri1 = new URIParser(uri1).stringify()
  uri2 = new URIParser(uri2).stringify()
  return uri1 === uri2
}

exports.URIParser = URIParser